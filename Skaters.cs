﻿using System;

namespace NETProgrammingProject
{
    public class Skaters
    {
        private float _finalScore;
        public string FirstSkaterName { get; set; }
        public string SecondSkaterName { get; set; }
        public string Country { get; set; }
        public float[] TechnicalAspectsArray { get; set; }
        public float[] PerformanceAspectsArray { get; set; }

        public float GetFinalScore()
        {
            return _finalScore;
        }

        public void CalculateFinalScore()
        {
            float average1 = 0;
            float average2 = 0;
            float result = 0;
            foreach (var tempTech in TechnicalAspectsArray)
            {
                average1 += tempTech;
            }

            average1 = average1 / 8;
            result += average1;

            foreach (var tempPerf in PerformanceAspectsArray)
            {
                average2 += tempPerf;
            }

            average2 = average2 / 8;
            result += average2;
            float fc = (float)Math.Round(result * 10f) / 10f;
            this._finalScore = fc;
        }

    }
}