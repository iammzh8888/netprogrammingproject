﻿using System;
using System.Collections.Generic;
using System.IO;

namespace NETProgrammingProject
{
    class SkatersDemo
    {
        private static readonly int _maxNumber = 10;
        static void Main(string[] args)
        {
            string filePath = "C:\\temp\\Pairs.txt";
            Skaters[] skaters = GetSkaters(filePath);
            SortDescendingSkaters(ref skaters);
            DisplayResults(skaters);
            GiveMedalWinners(skaters);
        }

        private static Skaters[] GetSkaters(string filePath)
        {
            Skaters[] skatersArray = new Skaters[_maxNumber];
            int len = skatersArray.Length;
            for (int x = 0; x < len; x++)
            {
                skatersArray[x] = new Skaters();
            }
            
            try
            {
                StreamReader streamReader = new StreamReader(filePath);
                string line = streamReader.ReadLine();
                while (line != null)
                {
                    for (var i = 0; i < _maxNumber; i++)
                    {
                        for (var j = 0; j < 5; j++)
                        {
                            switch (j)
                            {
                                case 0:
                                    skatersArray[i].FirstSkaterName = line;
                                    break;
                                case 1:
                                    skatersArray[i].SecondSkaterName = line;
                                    break;
                                case 2:
                                    skatersArray[i].Country = line;
                                    break;
                                case 3:
                                    skatersArray[i].TechnicalAspectsArray =
                                        Array.ConvertAll(line.Split(" "), float.Parse);
                                    break;
                                case 4:
                                    skatersArray[i].PerformanceAspectsArray =
                                        Array.ConvertAll(line.Split(" "), float.Parse);
                                    break;
                            }
                            line = streamReader.ReadLine();
                        }
                        skatersArray[i].CalculateFinalScore();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }

            return skatersArray;
        }

        private static void SortDescendingSkaters(ref Skaters[] skaters)
        {
            int arrayLen = skaters.Length;
            for (var i = 0; i < arrayLen - 1; i++)
            {
                for (var j = i + 1; j < arrayLen; j++)
                {
                    if (skaters[i].GetFinalScore() < skaters[j].GetFinalScore())
                    {
                        SwapSkaters(ref skaters[i], ref skaters[j]);
                    }
                }
            }

        }

        private static void SwapSkaters(ref Skaters skaters1, ref Skaters skaters2)
        {
            Skaters tempSkater = skaters2;
            skaters2 = skaters1;
            skaters1 = tempSkater;
        }

        private static void DisplayResults(Skaters[] skaters)
        {
            Console.WriteLine();
            Console.WriteLine($"{"Ranking",-9}{"1st Skater Name",-17}{"2nd Skater Name",-17}{"Country",-13}{"Final Score",-11}");
            int arrayLen = skaters.Length;
            for (var i = 0; i < arrayLen; i++)
            {
                Console.WriteLine($"{i+1,-9}{skaters[i].FirstSkaterName,-17}{skaters[i].SecondSkaterName,-17}{skaters[i].Country,-13}{skaters[i].GetFinalScore(),-11}");
            }
        }

        private static void GiveMedalWinners(Skaters[] skaters)
        {
            List<int> goldList = new List<int>();
            goldList.Add(0);
            bool isGold = true;
            bool isSilver = false;
            bool isBronze = false;
            List<int> silverList = new List<int>();
            List<int> bronzeList = new List<int>();
            int len = skaters.Length;
            for (var i = 1; i < len; i++)
            {
                if (isGold == true || isSilver == true || isBronze == true)
                {
                    if (isGold == true)
                    {
                        if (skaters[i].GetFinalScore().Equals(skaters[i - 1].GetFinalScore()))
                        {
                            goldList.Add(i);
                        }
                        else
                        {
                            if (i >= 3)
                                break;
                            silverList.Add(i);
                            isGold = false;
                            isSilver = true;
                        }
                    }
                    else if (isSilver == true)
                    {
                        if (skaters[i].GetFinalScore().Equals(skaters[i - 1].GetFinalScore()))
                        {
                            silverList.Add(i);
                        }
                        else
                        {
                            if (i >= 3)
                                break;
                            bronzeList.Add(i);
                            isSilver = false;
                            isBronze = true;
                        }
                    }
                    else if (isBronze == true)
                    {
                        if (skaters[i].GetFinalScore().Equals(skaters[i - 1].GetFinalScore()))
                        {
                            bronzeList.Add(i);
                        }
                        else
                        {
                            if (i >= 3)
                                break;
                            isBronze = false;
                        }
                    }
                }
            }

            Console.WriteLine();
            Console.WriteLine($"{"Medal",-8}{"1st Skater Name",-17}{"2nd Skater Name",-17}{"Country",-13}{"Final Score",-11}");

            foreach (var tempIndex in goldList)
            {
                Console.WriteLine($"{"Gold",-9}{skaters[tempIndex].FirstSkaterName,-17}{skaters[tempIndex].SecondSkaterName,-17}{skaters[tempIndex].Country,-13}{skaters[tempIndex].GetFinalScore(),-11}");
            }

            if (silverList.Count > 0)
            {
                foreach (var tempIndex in silverList)
                {
                    Console.WriteLine($"{"Silver",-9}{skaters[tempIndex].FirstSkaterName,-17}{skaters[tempIndex].SecondSkaterName,-17}{skaters[tempIndex].Country,-13}{skaters[tempIndex].GetFinalScore(),-11}");
                }
            }

            if (bronzeList.Count > 0)
            {
                foreach (var tempIndex in bronzeList)
                {
                    Console.WriteLine(
                        $"{"Bronze",-9}{skaters[tempIndex].FirstSkaterName,-17}{skaters[tempIndex].SecondSkaterName,-17}{skaters[tempIndex].Country,-13}{skaters[tempIndex].GetFinalScore(),-11}");
                }
            }

        }
    }
}
